// client

#include "../resmgr/resmgr.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>


int main()
{
	int resmgr;								// ���������� ����������
	int rc;

	ECDataExchangeStruct Data;				// ��������� � ������� ��� ��


	resmgr = open (RESMGR_PATH, O_RDONLY);	// ����������� � ��
	if (resmgr == -1)  {					// ��������� ������ ����������
		printf ("Can not open resmgr\n");
		exit (EXIT_FAILURE);
	}

	// ������������ ������ ��� ��:
	Data.outErrCode = 0;

	printf ("Input digital thread: ");
	char line[100];
	gets(line);								// ���� ������

	int i = 0;
	while ((line[i]<='9')&&(line[i]>='0'))	// �������������� �������� ������ � ������ unsigned char
	{
		char c = line[i];
		Data.inOutData[i]=atoi(&c);
		i++;
	}
	Data.inOutData[i]=10;					// ��� ��������� ����� ������, ����� ������ > 9

	printf ("\nInput code (1 - Aiken, 2 - Gray, 3 - Excess): ");
	int Var = 0;
	scanf("%d", &Var);						// ����� �������� �����������

	unsigned int CodeVariant;				// ������������� ������� �����������

	switch(Var)								// ����� ������������� ������� �����������
	{
	case 1:
		CodeVariant = ES_CTL_CODE_AIKEN;
		break;
	case 2:
		CodeVariant = ES_CTL_CODE_GRAY;
		break;
	case 3:
		CodeVariant = ES_CTL_CODE_3EXCESS;
		break;
	default :
		CodeVariant = ES_CTL_CODE_AIKEN;
	}

	printf ("\nSend: ");					// ������� �� ����� ������������ ������
	for(int j=0;j<i;j++)
			printf ("%i ",Data.inOutData[j]);

	rc = devctl (resmgr, CodeVariant, &Data,	// �������� ������ ��
				 sizeof (Data), NULL);
	if (rc != EOK) {
		printf ("Error in resmgr call\n");
		exit (EXIT_FAILURE);
	}

	printf ("\nReceived from resmgr: ");
	i=0;
	while (Data.inOutData[i]<10)			// ������� �� ����� ���������� ������ �� ��
	{
		printf ("%u ", Data.inOutData[i]);
		i++;
	}
	printf ("\n");

	close (resmgr);							// ������� ����������

	exit (EXIT_SUCCESS);
}
