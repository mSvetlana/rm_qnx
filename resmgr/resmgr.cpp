// resmgr.cpp

#include "resmgr.h"

#include "rm_main.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


// ---------------------------------------------------------
// �������������
char*
InitResmgr (int argc, char** argv, resmgr_parms_t* resmgr_parms,
            callbacks_t* callbacks)
{
	callbacks->IODevctl = IODevctl;

	return RESMGR_PATH;
}

// --------------------------------------------------------
// ������� ����������
void
CloseResmgr (dispatch_t* dpp, void* attrdata)
{
}

// -----------------------------------------------------------
// ���������� Devctl
int
IODevctl (resmgr_context_t* ctp, io_devctl_t* msg, RESMGR_OCB_T* ocb)
{
	int   rc = ENOSYS;					// ������������ ��� ������
	int   nbytes;						// ������ ������
	void* data;							// ���������� ������
	int	  Variant;						// �������� ����������� (����� ������� �������)


	ECDataExchangeStruct* Msg;			// ��������� �������

	data = _DEVCTL_DATA (msg->i);
	nbytes = 0;
	unsigned char table[10][3][4] =
	{		//�����		� ��������	����
			{{0,0,0,0},	{0,0,1,1},	{0,0,0,0}},	// 0
			{{0,0,0,1},	{0,1,0,0},	{0,0,0,1}},	// 1
			{{0,0,1,0},	{0,1,0,1},	{0,0,1,1}},	// 2
			{{0,0,1,1},	{0,1,1,0},	{0,0,1,0}},	// 3
			{{0,1,0,0},	{0,1,1,1},	{0,1,1,0}},	// 4
			{{1,0,1,1},	{1,0,0,0},	{0,1,1,1}},	// 5
			{{1,1,0,0},	{1,0,0,1},	{0,1,0,1}},	// 6
			{{1,1,0,1},	{1,0,1,0},	{0,1,0,0}},	// 7
			{{1,1,1,0},	{1,0,1,1},	{1,1,0,0}},	// 8
			{{1,1,1,1},	{1,1,0,0},	{1,1,0,1}} 	// 9
	};



	switch (msg->i.dcmd)
	{
	case ES_CTL_CODE_AIKEN:		// ����������� ������������ � �������������� ���� ������.
		Variant = 0;
		rc = EOK;
		break;

	case ES_CTL_CODE_GRAY:		// ����������� ������������ � �������������� ���� ����.
		Variant = 2;
		rc = EOK;
		break;

	case ES_CTL_CODE_3EXCESS:	// ����������� ������������ � �������������� ���� � �������� 3.
		Variant = 1;
		rc = EOK;
		break;

	default:
		rc = ENOSYS;			// ����������� �������
	}

	if (rc == EOK)
	{
		Msg = (ECDataExchangeStruct*) data;

		printf ("Received from client: ");
		int i = 0;
		while (Msg->inOutData[i]<10)					// ����� ���������� �� ������� ���������
			printf ("%u ", Msg->inOutData[i++]);


		unsigned char buf[100];							// ��������������� ������ ��� ������������ ������
		i=0;
		while(Msg->inOutData[i]<10)						// ��������� �����
		{
			for(int j=0;j<4;j++)
				buf[i*4+j] = table[Msg->inOutData[i]][Variant][j];
			i++;
		}
		buf[i*4] = 10;									// ��������� ����� ��������� ���������

		printf ("\nRes: ");								// ����� �� ����� ��������������� ������ �������
		i = 0;
		while (buf[i]<10)
			printf ("%u ", buf[i++]);
		printf ("\n");

		memcpy(Msg->inOutData,buf,i*4+1);				// �������� ��������� �� ���������������� ������� � ��������� �������
		Msg->outErrCode = 0;
	}
	else
	{
		Msg->outErrCode = rc;							// ������ ��� ������
	}


	// ��������� �������� ���������:
	nbytes = sizeof (ECDataExchangeStruct);

	memset (&msg->o, 0, sizeof (msg->o));

	msg->o.ret_val = EOK;
	msg->o.nbytes  = nbytes;

	rc = _RESMGR_PTR (ctp, &msg->o, sizeof (msg->o) + nbytes);

	return rc;
}
