#ifndef RM_MAIN_H
#define RM_MAIN_H

struct extattr_t;
#define IOFUNC_ATTR_T struct extattr_t

struct extocb_t;
#define IOFUNC_OCB_T  struct extocb_t

#include <sys/iofunc.h>
#include <sys/dispatch.h>

struct extattr_t {
	iofunc_attr_t   attr;
	iofunc_notify_t notify [3];
	void*           data;
};

struct extocb_t {
	iofunc_ocb_t ocb;
	void*        data;
};

typedef struct {
	mode_t   mode;
	unsigned nparts_max;
	unsigned msg_max_size;
	void*    attrdata;
}	resmgr_parms_t;

typedef struct {
	int   (*Attach)    (dispatch_t* dpp, IOFUNC_ATTR_T* attr);
	void* (*OcbCalloc) (resmgr_context_t* ctp, IOFUNC_ATTR_T* attr);
	void  (*OcbFree)   (IOFUNC_OCB_T* ocb);
	int   (*IODevctl)  (resmgr_context_t* ctp, io_devctl_t* msg,
	                    RESMGR_OCB_T* ocb);

}	callbacks_t;

char* InitResmgr (int argc, char** argv, resmgr_parms_t* resmgr_parms,
                  callbacks_t* callbacks);
void  CloseResmgr (dispatch_t* dpp, void* attrdata);

int   Attach    (dispatch_t* dpp, IOFUNC_ATTR_T* attr);
void* OcbCalloc (resmgr_context_t* ctp, IOFUNC_ATTR_T* attr);
void  OcbFree   (IOFUNC_OCB_T* ocb);
int   IODevctl  (resmgr_context_t* ctp, io_devctl_t* msg, RESMGR_OCB_T* ocb);

#endif
