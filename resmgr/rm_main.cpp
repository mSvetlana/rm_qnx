#include "rm_main.h"

#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>


#define ERR_MSG  "Can not initialize resource manager"

static resmgr_parms_t resmgr_parms = {
	mode         : S_IFNAM | 0,
	nparts_max   : 1,
	msg_max_size : 2048,
	attrdata     : NULL
};

static callbacks_t callbacks;

static IOFUNC_OCB_T* ocb_calloc (resmgr_context_t* ctp, IOFUNC_ATTR_T* attr);
static void ocb_free (IOFUNC_OCB_T* ocb);

static iofunc_funcs_t ocb_funcs = {
	_IOFUNC_NFUNCS,
	ocb_calloc,
	ocb_free
};

static iofunc_mount_t mountpoint = {0, 0, 0, 0, &ocb_funcs};

static dispatch_t*         dpp = NULL;
static int                 id  = -1;
static dispatch_context_t* ctp = NULL;

static resmgr_connect_funcs_t connect_funcs;
static resmgr_io_funcs_t      io_funcs;
static IOFUNC_ATTR_T          attr;

static char* path = NULL;

static void Initialization (int argc, char** argv);
static void MainLoop ();
static void Completing ();
static void ResmgrAttach (char* path, resmgr_parms_t* resmgr_parms);
static void ChangeRMFunctions (resmgr_connect_funcs_t* connect_funcs,
                               resmgr_io_funcs_t*      io_funcs);

static int io_close_ocb (resmgr_context_t* ctp, void* reserved,
                      RESMGR_OCB_T* ocb);

static int io_devctl (resmgr_context_t* ctp, io_devctl_t* msg,
                      RESMGR_OCB_T* ocb);

// ---------------------------------------------------------------> main <---
int
main (int argc, char** argv)
{
	Initialization (argc, argv);
	MainLoop ();

	return EXIT_SUCCESS;
}

// -----------------------------------------------------> Initialization <---
static void
Initialization (int argc, char** argv)
{
	int rc;

	atexit (Completing);

	path = InitResmgr (argc, argv, &resmgr_parms, &callbacks);
							if (path == NULL) errx (1,
							ERR_MSG " (init)");

	dpp = dispatch_create ();
							if (dpp == NULL) err (1,
							ERR_MSG " (create)");

	ResmgrAttach (path, &resmgr_parms);

	if (callbacks.Attach != NULL) {
		rc = callbacks.Attach (dpp, &attr);
							if (rc != EOK) errx (1,
							ERR_MSG " (attach)");
	}

	ctp = dispatch_context_alloc (dpp);
							if (ctp == NULL) err (1,
							ERR_MSG " (context)");
}

// -----------------------------------------------------------> MainLoop <---
static void
MainLoop ()
{
	while (1) {
		ctp = dispatch_block (ctp);
		if (ctp == NULL) err (1, "Resource manager block error");
		dispatch_handler (ctp);
	}
}

// ---------------------------------------------------------> Completing <---
static void
Completing ()
{
	CloseResmgr (dpp, attr.data);

	if (ctp != NULL)
		dispatch_context_free (ctp);

	if (id != -1)
		resmgr_detach (dpp, id, _RESMGR_DETACH_ALL);

	if (dpp != NULL)
		dispatch_destroy (dpp);
}

// -------------------------------------------------------> ResmgrAttach <---
static void
ResmgrAttach (char* path, resmgr_parms_t* resmgr_parms)
{
	resmgr_attr_t resmgr_attr;

	iofunc_attr_init ((iofunc_attr_t*) &attr,
	                  resmgr_parms->mode, NULL, NULL);

	attr.data = resmgr_parms->attrdata;
	attr.attr.mount = &mountpoint;
	IOFUNC_NOTIFY_INIT (attr.notify);

	iofunc_func_init (_RESMGR_CONNECT_NFUNCS, &connect_funcs,
	                  _RESMGR_IO_NFUNCS,      &io_funcs);

	ChangeRMFunctions (&connect_funcs, &io_funcs);

	memset (&resmgr_attr, 0, sizeof (resmgr_attr));
	resmgr_attr.nparts_max   = resmgr_parms->nparts_max;
	resmgr_attr.msg_max_size = resmgr_parms->msg_max_size;

	id = resmgr_attach (dpp, &resmgr_attr, path, _FTYPE_ANY, 0,
	                    &connect_funcs, &io_funcs, &attr);
							if (id == -1) err (1,
							ERR_MSG " (resmgr)");
}

// --------------------------------------------------> ChangeRMFunctions <---
static void
ChangeRMFunctions (resmgr_connect_funcs_t* connect_funcs,
                   resmgr_io_funcs_t*      io_funcs)
{
	io_funcs->close_ocb = io_close_ocb;

	if (callbacks.IODevctl != NULL) io_funcs->devctl = io_devctl;
}

// ---------------------------------------------------------> ocb_calloc <---
static IOFUNC_OCB_T*
ocb_calloc (resmgr_context_t* ctp, IOFUNC_ATTR_T* attr)
{
	IOFUNC_OCB_T* ocb;
	void*         ocbdata;

	ocb = (IOFUNC_OCB_T*) calloc (1, sizeof (IOFUNC_OCB_T));

	if (ocb == NULL) {
		warn ("Can not allocate ocb");
		return NULL;
	}

	if (callbacks.OcbCalloc == NULL)
		return ocb;

	ocbdata = callbacks.OcbCalloc (ctp, attr);

	if (ocbdata == NULL) {
		free (ocb);
		return NULL;
	}

	ocb->data = ocbdata;

	return ocb;
}

// -----------------------------------------------------------> ocb_free <---
static void
ocb_free (IOFUNC_OCB_T* ocb)
{
	if (callbacks.OcbFree != NULL)
		callbacks.OcbFree (ocb);

	free (ocb);
}

// -------------------------------------------------------> io_close_ocb <---
static int
io_close_ocb (resmgr_context_t* ctp, void* reserved, RESMGR_OCB_T* ocb)
{
	int rc;

// --- insert your code here ---

// --- end of your code ---

	iofunc_notify_remove (ctp, ocb->ocb.attr->notify);

	rc = iofunc_close_ocb_default (ctp, reserved, (iofunc_ocb_t*) ocb);

	return rc;
}


// ----------------------------------------------------------> io_devctl <---
static int
io_devctl (resmgr_context_t* ctp, io_devctl_t* msg, RESMGR_OCB_T* ocb)
{
	int rc;

	rc = iofunc_devctl_default (ctp, msg, (iofunc_ocb_t*) ocb);

	if (rc != (int) _RESMGR_DEFAULT)
			return rc;

// --- insert your code here ---

	rc = callbacks.IODevctl (ctp, msg, ocb);

// --- end of your code ---

	return rc;
}

