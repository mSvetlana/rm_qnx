// ===========================================================> resmgr.h <===

#ifndef RESMGR_H
#define RESMGR_H

#include <devctl.h>

#define RESMGR_PATH  "/dev/resmgr"

struct ECDataExchangeStruct		// ��������� ���������, ������������ ����� �� � ��������
{
	//unsigned char *inOutData; 	// �������� �����
	unsigned char inOutData[100]; 	// �������� �����
	int outErrCode; 			// ��� ������
};

// ���� ������
#define ES_CTL_CODE_AIKEN  	__DIOTF (_DCMD_MISC, 1, ECDataExchangeStruct)	// ����������� ������������ � �������������� ���� ������.
#define ES_CTL_CODE_GRAY  	__DIOTF (_DCMD_MISC, 2, ECDataExchangeStruct)	// ����������� ������������ � �������������� ���� ����.
#define ES_CTL_CODE_3EXCESS __DIOTF (_DCMD_MISC, 3, ECDataExchangeStruct)	// ����������� ������������ � �������������� ���� � �������� 3.

#endif
